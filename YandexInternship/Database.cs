﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;

namespace YandexInternship
{
    public class Database
    {
        string connString;

        public Database(string connString)
        {
            this.connString = connString;
        }

        public void PrepareDatabase()
        {

            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();

                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = "DROP TABLE IF EXISTS orders;" +
                    "CREATE TABLE orders(" +                                  //order является зарезервированным словом https://sqlite.org/lang_keywords.html
                    "id INTEGER PRIMARY KEY, " +                              //Предполагает что id должен быть primary key, хотя этого не было в описании таблицы. Autoincrement не критичен.
                    "dt INTEGER NOT NULL, " +                                 //SQLite не имеет временного типа, поэтому оно хранится в инте как отступ от 1970г
                    "product_id INTEGER NOT NULL," +
                    "amount REAL NOT NULL);" +
                    "DROP TABLE IF EXISTS product;" +
                    "CREATE TABLE product(" +
                    "id INTEGER," +
                    "name TEXT);" +
                    "INSERT INTO product(id, name) VALUES (1, 'A');" +
                    "INSERT INTO product(id, name) VALUES (2, 'B');" +
                    "INSERT INTO product(id, name) VALUES (3, 'C');" +
                    "INSERT INTO product(id, name) VALUES (4, 'D');" +
                    "INSERT INTO product(id, name) VALUES (5, 'E');" +
                    "INSERT INTO product(id, name) VALUES (6, 'F');" +
                    "INSERT INTO product(id, name) VALUES (7, 'G');";
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка при подготовке базы данных: {e.Message}\nПожалуйста, перезапустите приложение");
                    Console.WriteLine($"Ошибка при добавлении случайных данных: {e.Message}\nПожалуйста, перезапустите приложение");
                }
            }
        }

        public void CreateRandomData(int num)
        {
            Console.WriteLine($"Создание и загрузка случайных данных");
            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                var tr = conn.BeginTransaction();
                cmd.Transaction = tr;

                Random r = new Random(1000);            //seed неважен, главное чтобы результат повторялся для удобства дебага
                int n = num >= 100 ? num / 100 : 1;     //Если разбить все строки на 100 частей, то n это количество строк в одной части. Нужно для отображения прогресса.
                for (int i = 0; i < num; i++)
                {
                    if (i % n == 0) Console.WriteLine($"\t{100 * i / num}% of work. {i} random values inserted");
                    cmd.CommandText = "INSERT INTO orders(dt, product_id, amount) VALUES (@dt, @product_id, @amount);";
                    cmd.Parameters.AddWithValue("dt", r.Next(1483228800, 1493004800)); //заметка: 1483228800 дата в int'е для 2017-01-01T00:00:00, 1491004800 для 2017-04-01T00:00:00
                    cmd.Parameters.AddWithValue("product_id", r.Next(1, 8));
                    cmd.Parameters.AddWithValue("amount", r.Next(1, 100000));

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Debug.Print($"Ошибка при добавлении случайных данных: {e.Message}");
                        Console.WriteLine($"Ошибка при добавлении случайных данных: {e.Message}");
                    }
                }
                try
                {
                    tr.Commit();
                    tr.Dispose();
                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка транзакции: {e.Message}");
                    Console.WriteLine($"Ошибка транзакции: {e.Message}");
                }
                Console.WriteLine("Random data inserted");
            }
        }

        internal void InsertDataFromFile(string filename)
        {
            Console.WriteLine($"Загрузка данных из файла {filename}:");
            StreamReader file;
            if (File.Exists(filename)) { file = new StreamReader(filename); }
            else
            {
                Console.WriteLine("File does not exists");
                return;
            }

            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                var tr = conn.BeginTransaction();
                cmd.Transaction = tr;

                string[] fullFile = file.ReadToEnd().Split('\n');           //Условимся что перенос строки \n а не \r\n
                string[] split_cache;                                       //понадобится когда будем разбивать строку на переменные. Чтобы не делать по 4 раза сплит на одну строку
                int line_num = fullFile.Length;                             //long здесь врятли понадобится, оставляю инт
                int n = line_num >= 100 ? line_num / 100 : 1;               //Если разбить все строки на 100 частей, то n это количество строк в одной части. Нужно для отображения прогресса.

                int i = 0;
                foreach (string line in fullFile)
                {
                    i++;
                    split_cache = line.Split('\t');

                    if (split_cache[0] == "") { Console.WriteLine($"Ошибка в строке {i}. Отсутствует id"); continue; }  //Предполагается, что если элемент отсутствует,
                    if (split_cache[1] == "") { Console.WriteLine($"Ошибка в строке {i}. Отсутствует dt"); continue; }  //то табуляция всё равно расставлена правильно (как в примере из ТЗ)
                    if (split_cache[2] == "") { Console.WriteLine($"Ошибка в строке {i}. Отсутствует product_id"); continue; }
                    if (split_cache[3] == "") { Console.WriteLine($"Ошибка в строке {i}. Отсутствует amount"); continue; }

                    int id; //В идеале, объявление переменных лучше вынести из цикла, но тут выглядит понятнее.
                    if (!int.TryParse(split_cache[0], out id)) { Console.WriteLine($"Ошибка в строке {i}. Некорректный формат id"); continue; }
                    DateTime dt;
                    if (!DateTime.TryParse(split_cache[1], out dt)) { Console.WriteLine($"Ошибка в строке {i}. Некорректный формат dt"); continue; }
                    float amount;
                    if (!float.TryParse(split_cache[2].Replace('.', ','), out amount)) { Console.WriteLine($"Ошибка в строке {i}. Некорректный формат amount"); continue; }
                    int product_id;
                    if (!int.TryParse(split_cache[3], out product_id)) { Console.WriteLine($"Ошибка в строке {i}. Некорректный формат product_id"); continue; }


                    if (i % n == 0) Console.WriteLine($"\t{100 * i / line_num}% of work. {i} values inserted");
                    cmd.CommandText = "INSERT INTO orders(id, dt, product_id, amount) VALUES (@id, strftime('%s', @dt), @product_id, @amount);"; //(strftime('%s', @dt) переводит в базе время в инт.
                    cmd.Parameters.AddWithValue("id", id);
                    cmd.Parameters.AddWithValue("dt", split_cache[1]);         //SQLite сама умеет преобразовывать время из строки в инт, когда формат корректен.
                    cmd.Parameters.AddWithValue("product_id", product_id);
                    cmd.Parameters.AddWithValue("amount", amount);

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Debug.Print($"Ошибка при добавлении строки {i} из файла: {e.Message}");
                        Console.WriteLine($"Ошибка при добавлении строки {i} из файла: {e.Message}");
                    }
                }
                try
                {
                    tr.Commit();
                    tr.Dispose();
                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка транзакции: {e.Message}");
                    Console.WriteLine($"Ошибка транзакции: {e.Message}");
                }
                Console.WriteLine("Данные из файла добавлены");
            }
        }

        public void printAmountAndSumOfEachProduct()
        {
            Console.WriteLine("Количество и сумма по каждому продукту за текущий месяц:");

            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT product.name, count(product_id), sum(amount) " +
                    "FROM orders, product " +
                    "WHERE strftime('%m',datetime(dt,'unixepoch')) = strftime('%m',datetime()) and product_id = product.id " +
                    "GROUP BY product_id;";
                try
                {
                    using (SQLiteDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            Console.WriteLine($"{r["name"]}  {r["count(product_id)"]} {r["sum(amount)"]}");
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка в процессе запроса: {e.Message}");
                    Console.WriteLine($"Ошибка в процессе запроса: {e.Message}");
                }
            }
        }


        internal void printProductsOrderedInCurrentButNotInPast()
        {
            Console.WriteLine("Продукты, которые были заказаны в текущем месяце, но не в прошлом:");

            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                HashSet<string> prev_month = new HashSet<string>();

                //Проверяющему: Можно было постараться сделать всё одним запросом, но он был бы совершенно непонятен. Поэтому в два + код.
                try
                {
                    //Собираем айди продуктов, которые были в прошлом месяце. +0 нужен так как -1 вызывает приведение строки к инту у правой части сравнения
                    cmd.CommandText = "SELECT DISTINCT product.name " +
                        "FROM orders, product " +
                        "WHERE strftime('%m',datetime(dt,'unixepoch'))+0 = strftime('%m',datetime())-1 and orders.product_id = product.id;";

                    using (SQLiteDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            prev_month.Add(r["name"].ToString());
                        }
                    }

                    //Теперь берём айди текущего месяца и проверяем были ли они в прошлом.
                    cmd.CommandText = "SELECT DISTINCT product.name " +
                        "FROM orders, product " +
                        "WHERE strftime('%m',datetime(dt,'unixepoch')) = strftime('%m',datetime()) and orders.product_id = product.id;";

                    using (SQLiteDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            if (!prev_month.Contains(r["name"].ToString()))
                                Console.WriteLine(r["name"].ToString());
                        }
                    }

                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка в процессе запроса: {e.Message}");
                    Console.WriteLine($"Ошибка в процессе запроса: {e.Message}");
                }
            }
        }

        internal void printProductsOrderedInCurrentButNotInPastAndVersa()
        {
            Console.WriteLine("Продукты, которые были заказаны либо текущем месяце, либо в прошлом:");

            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                HashSet<string> prev_month = new HashSet<string>();
                HashSet<string> cur_month = new HashSet<string>();
                
                try
                {
                    //Собираем айди продуктов, которые были в прошлом месяце. +0 нужен так как -1 вызывает приведение строки к инту у правой части сравнения
                    cmd.CommandText = "SELECT DISTINCT product.name " +
                        "FROM orders, product " +
                        "WHERE strftime('%m',datetime(dt,'unixepoch'))+0 = strftime('%m',datetime())-1 and orders.product_id = product.id;";
                    using (SQLiteDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            prev_month.Add(r["name"].ToString());
                        }
                    }

                    //Теперь берём айди текущего месяца и проверяем были ли они в прошлом, попутно добавляя значения в сет текущего месяца.
                    cmd.CommandText = "SELECT DISTINCT product.name " +
                        "FROM orders, product " +
                        "WHERE strftime('%m',datetime(dt,'unixepoch')) = strftime('%m',datetime()) and orders.product_id = product.id;";
                    using (SQLiteDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            cur_month.Add(r["name"].ToString());
                            if (!prev_month.Contains(r["name"].ToString()))
                                Console.WriteLine(r["name"].ToString());
                        }
                    }

                    foreach(string key in prev_month)
                    {
                        if (!cur_month.Contains(key))
                            Console.WriteLine(key);
                    }

                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка в процессе запроса: {e.Message}");
                    Console.WriteLine($"Ошибка в процессе запроса: {e.Message}");
                }
            }
        }

        internal void printTopSumsOfEachMonths()
        {
            Console.WriteLine("Отчёт по максимально заказываемому продукту в разрезе месяцев");

            using (SQLiteConnection conn = new SQLiteConnection(this.connString))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT datetime(dt,'unixepoch') as dt, product.name, max(amount), sum(amount) FROM orders, product WHERE orders.product_id = product.id GROUP BY strftime('%m',datetime(dt,'unixepoch')), orders.product_id ORDER BY strftime('%m',datetime(dt,'unixepoch')), sum(amount) DESC;";

                SQLiteCommand cmdsumm = conn.CreateCommand();
                cmdsumm.CommandText = "SELECT sum(amount) FROM orders WHERE strftime('%m',datetime(dt,'unixepoch')) = strftime('%m',@month) GROUP BY strftime('%m',datetime(dt,'unixepoch'));";

                string month, prevmonth = "";
                int percent;
                float sum;
                try
                {
                    using (SQLiteDataReader r = cmd.ExecuteReader())
                    {
                        Console.WriteLine("  Период  Продукт    Сумма     Доля");
                        while (r.Read())
                        {
                            month = DateTime.Parse(r["dt"].ToString()).ToString("MMM yyyy");
                            cmdsumm.Parameters.AddWithValue("month", r["dt"].ToString());

                            if (!month.Equals(prevmonth))
                            {
                                prevmonth = month;
                            }
                            else
                            {
                                continue;
                            }

                            sum = float.Parse(cmdsumm.ExecuteScalar().ToString());
                            percent = (int)(float.Parse(r["max(amount)"].ToString()) / sum * 100);
                            Console.WriteLine($"{month,8} {r["name"],8} {r["sum(amount)"],8:N0} {percent,8}");
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Print($"Ошибка в процессе запроса: {e.Message}");
                    Console.WriteLine($"Ошибка в процессе запроса: {e.Message}");
                }
            }
        }
    }
}
