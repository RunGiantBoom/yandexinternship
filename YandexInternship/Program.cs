﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
//using static YandexInternship.DatabaseAccess;

//Есть две хороших библиотеки в Nuget.
//1) System.Data.SQLite
//2) sqlite-net
// Первая тянет в зависимостях EntityFramework, вторая используя рефлексии выглядит как EntityFramework.
// Думаю что запрет на Entity вызван её подходом Code First и надо больше думать о том как составлять запросы к базе.
// Поэтому выбран для работы System.Data.SQLite, а запросы будут реализованы через селекты.

namespace YandexInternship
{
    class Program
    {
        static void Main(string[] args)
        {
            Database db = new Database("Data Source=filename.db; Version=3;New=True;");
            db.PrepareDatabase();
            //db.CreateRandomData(20000000);         //20 миллионов значений. 400 мегабайт база. Обрабатывается нормально.
            if (args.Length > 0)
            {
                db.InsertDataFromFile(args[0]);
            }
            else
            {
                Console.WriteLine("Файл с данными не определён.\nПерезапустите программу указав название файла в качестве первого аргумента в командной строке.");
                return;
            }


                Console.WriteLine();
            Console.WriteLine("Запросы:");

            //жесть какая. Нужен явно другой подход для наименования.
            db.printAmountAndSumOfEachProduct();                    //Количество и сумма по каждому продукту
            db.printProductsOrderedInCurrentButNotInPast();         //Выводит в консоль все продукты которые были заказаны в текущем месяце, но которых не было в прошлом.
            db.printProductsOrderedInCurrentButNotInPastAndVersa(); //повторяет предыдущий но с теми которые были либо в прошлом месяце, либо в этом.
            db.printTopSumsOfEachMonths();                          //Вывод самый максимальнго по сумме продукта за каждый месяц, с долей от объема за месяц
        }
    }
}
